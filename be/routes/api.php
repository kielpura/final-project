<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminAuthController;
use App\Http\Controllers\AdminBookController;
use App\Http\Controllers\AdminAccountApprovalController;
use App\Http\Controllers\UserBookController;
use App\Http\Controllers\UserAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    Route::group(['prefix' => 'admin'], function (){
        Route::post('login', [AdminAuthController::class, 'login']);
        Route::post('store', [AdminAuthController::class, 'store']);
        Route::post('logout', [AdminAuthController::class, 'logout']);
        Route::post('me', [AdminAuthController::class, 'me']);
    });

    Route::group(['prefix' => 'user'], function (){
        Route::post('login', [UserAuthController::class, 'login']);
        Route::post('store', [UserAuthController::class, 'store']);
        Route::post('logout', [UserAuthController::class, 'logout']);
        Route::post('me', [UserAuthController::class, 'me']);
    });
});

Route::group(['middleware' => 'api'], function (){

    Route::group(['prefix' => 'admin'], function (){
        Route::get('books', [AdminBookController::class, 'books']);
        Route::get('borrowedbooks', [AdminBookController::class, 'borrowedBooks']);
        Route::get('categories', [AdminBookController::class, 'categories']);
        Route::get('mostborrowed', [AdminBookController::class, 'mostborrowed']);
        Route::post('search', [AdminBookController::class, 'searchBook']);
        Route::post('newbook', [AdminBookController::class, 'addBook']);
        Route::put('books/{id}', [AdminBookController::class, 'updateBook']);
        Route::delete('books/destroy/{id}', [AdminBookController::class, 'deleteBookRecord']);
        
        Route::get('newusers', [AdminAccountApprovalController::class, 'newUsers']);
        Route::put('users/{id}', [AdminAccountApprovalController::class, 'approveUser']);
        Route::delete('users/destroy/{id}', [AdminAccountApprovalController::class, 'deleteUser']);
    });

    Route::group(['prefix' => 'user'], function (){
        Route::get('books', [UserBookController::class, 'books']);
        Route::get('borrowedbooks', [UserBookController::class, 'borrowedBooks']);
        Route::get('categories', [UserBookController::class, 'categories']);
        Route::post('borrow', [UserBookController::class, 'borrowBook']);
        Route::delete('return/{id}', [UserBookController::class, 'returnBook']);
    });
});