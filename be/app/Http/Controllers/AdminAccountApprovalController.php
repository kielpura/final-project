<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserInfo;

class AdminAccountApprovalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function newUsers(){
        $data = User::with('userinfo')->where('account_type', 'Pending')->latest()->paginate(5);
        return response()->json($data);
    }

    public function approveUser(Request $request, $id){
        $data = User::where('id', $id)->first();
        $data->update([
            'account_type' => $request->account_type
        ]);
        return response()->json($data);
    }

    public function deleteUser($id){
        User::destroy($id);
        return response()->json($id);
    }
}