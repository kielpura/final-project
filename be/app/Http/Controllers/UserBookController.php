<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use App\Models\Category;

class UserBookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }

    public function books(){
        $book = Book::with('category:id,category')->get();
        return response()->json($book);
    }

    public function borrowedBooks(){
        $borrowedbooks = BorrowedBook::whereHas('user', function($query){
            $query->where('user_id', auth('user')->user()->id);
        })->with('user', 'book', 'user.userinfo')->latest()->paginate(5);
        return response()->json($borrowedbooks, 200);
    }

    public function categories(){
        $category = Category::all();
        return response()->json($category);
    }

    public function borrowBook(Request $request){
        $user = User::findOrFail(auth('user')->user()->id);
        
        $borrowbook = BorrowedBook::create([
            'user_id' => $user->id,
            'book_id' => $request->id,
        ]);
        
        return response()->json(['msg' => 'Borrowed successfully'], 200);
    }

    public function returnBook($id){
        BorrowedBook::destroy($id);
        return response()->json($id);
    }

    // public function borrowBook(Request $request){
    //     $user = User::findOrFail(auth('user')->user()->id);
    //     $book = Book::first();
        
    //     $borrowbook = BorrowedBook::create([
    //         'user_id' => $user->id,
    //         'book_id' => $request->id,
    //     ]);

    //     return response()->json(['msg' => 'Borrowed successfully'], 200);
    // }

    // public function returnBook(Request $request){
    //     $user = User::findOrFail(auth('user')->user()->id);
    //     $book = Book::first();
        
    //     $borrowbook = ReturnedBook::create([
    //         'user_id' => $user->id,
    //         'book_id' => $request->id,
    //         'copies' => $request->qty
    //     ]);

    //     $copies = Book::where('id', $request->id);

    //     if($copies){
    //         $copies->update(['copies' => ($request->copies + $request->qty)]);
    //     }
        
    //     return response()->json(['msg' => 'Borrowed successfully'], 200);
    // }
}
