<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use App\Http\Requests\UserAccountRequest;
use Illuminate\Support\Facades\Hash;

class UserAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user', ['except' => ['login', 'store']]);
    }
    
    public function login(Request $request)
    {
        if($token = auth()->guard('user')->attempt(['email' => $request->email, 'password' => $request->password, 'account_type' => 'Pending'])) {
            return response()->json(['msg' => 'Your account is not yet activated'], 403);
        }
        else if (! $token = auth()->guard('user')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        else
        {
            return $this->respondWithToken($token);
        }
    }

    public function store(UserAccountRequest $request){

        $userinfo = UserInfo::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'contact_number' => $request->contact_number,
        ]);
    
        $useraccount = User::create([
            'user_info_id' => $userinfo->id,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json(['msg' => 'Account created successfully!'], 200);
    }

    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    public function me()
    {
        $user = User::with(['userinfo'])->where('id', auth()->guard('user')->user()->id)->first();
        return response()->json($user);
    }

    protected function respondWithToken($token)
    {
        $user = User::where('id', auth('user')->user()->id)->first();
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('user')->factory()->getTTL() * 10080,
        ]);
    }
}
