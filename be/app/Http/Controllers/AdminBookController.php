<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BorrowedBook;
use App\Models\Book;
use App\Models\Category;

class AdminBookController extends Controller
{
    public function books(){
        $book = Book::with('category:id,category')->latest()->paginate(5);
        return response()->json($book);
    }

    public function borrowedBooks(){
        $borrowedbooks = BorrowedBook::with('user', 'book', 'user.userinfo')->latest()->paginate(5);
        return response()->json($borrowedbooks);
    }

    public function categories(){
        $category = Category::all();
        return response()->json($category);
    }

    public function addBook(Request $request){
        Book::create([
            'title' => $request->title,
            'author' => $request->author,
            'category_id' => $request->category_id,
        ]);
        return response()->json(['msg' => 'Book added successfully!'], 200);
    }

    public function updateBook(Request $request, $id){
        $data = Book::where('id', $id)->first();
        $data->update([
            'title' => $request->title,
            'author' => $request->author,
            'category_id' => $request->category_id,
                    ]);
        return response()->json($data);
    }

    public function deleteBookRecord($id){
        Book::destroy($id);
        return response()->json($id);
    }

    public function searchBook(Request $request){
        $book = Book::where('title', 'like', '%'.$request->search.'%')->paginate(5);
        return response()->json($book);
    }

    public function mostborrowed(){
        $mostborrowed = Category::with('borrowed')->withCount('borrowed')->get();
        return response()->json($mostborrowed);
    }
}
