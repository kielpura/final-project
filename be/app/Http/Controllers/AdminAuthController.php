<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\AdminInfo;
use Illuminate\Http\Request;
use App\Http\Requests\AdminAccountRequest;
use Illuminate\Support\Facades\Hash;

class AdminAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => ['login', 'store']]);
    }
    
    public function login(Request $request)
    {
            if (! $token = auth()->guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
            return $this->respondWithToken($token);
    }

    public function store(AdminAccountRequest $request){

        $admininfo = AdminInfo::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'contact_number' => $request->contact_number,
        ]);
    
        $adminaccount = Admin::create([
            'admin_info_id' => $admininfo->id,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json(['msg' => 'Account created successfully!'], 200);
    }

    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    public function me()
    {
        $user = Admin::with(['admininfo'])->where('id', auth()->guard('admin')->user()->id)->first();
        return response()->json($user);
    }

    protected function respondWithToken($token)
    {
        Admin::where('id', auth('admin')->user()->id)->first();
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('admin')->factory()->getTTL() * 10080,
        ]);
    }
}