<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Support\Facades\Hash;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_info = UserInfo::create([
            'first_name' => 'Angelica Marie',
            'middle_name' => 'Lacaba',
            'last_name' => 'Alejan',
            'gender' => 'Female',
            'contact_number' => '09455403661',
        ]);

        User::create([
            'email' => 'user@user.com',
            'password' => Hash::make('useruseruser'),
            'account_type' => 'Pending',
            'user_info_id' => $user_info->id
        ]);

        $user_info = UserInfo::create([
            'first_name' => 'Ezikiel',
            'middle_name' => 'Pura',
            'last_name' => 'Tulawan',
            'gender' => 'Male',
            'contact_number' => '09366036099',
        ]);

        User::create([
            'email' => 'user2@user.com',
            'password' => Hash::make('useruseruser'),
            'account_type' => 'Pending',
            'user_info_id' => $user_info->id
        ]);

    }
}
