<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;
use App\Models\AdminInfo;
use Illuminate\Support\Facades\Hash;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_info = AdminInfo::create([
            'first_name' => 'Ezikiel',
            'middle_name' => 'Pura',
            'last_name' => 'Tulawan',
            'gender' => 'Male',
            'contact_number' => '09366036099',
        ]);

        Admin::create([
            'email' => 'admin@admin.com',
            'password' => Hash::make('adminadmin'),
            'admin_info_id' => $admin_info->id
        ]);

    }
}