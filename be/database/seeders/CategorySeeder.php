<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'category' => 'Algebra'
        ]);
        Category::create([
            'category' => 'Biography'
        ]);
        Category::create([
            'category' => 'Dictionary'
        ]);
        Category::create([
            'category' => 'Encyclopedia'
        ]);
        Category::create([
            'category' => 'History'
        ]);
        Category::create([
            'category' => 'Philosophy'
        ]);
        Category::create([
            'category' => 'Science'
        ]);
        Category::create([
            'category' => 'Sports'
        ]);
    }
}
