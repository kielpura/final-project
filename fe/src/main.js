import Vue from 'vue'
import App from './App.vue'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import ViewUI from 'view-design';
import Toast from "vue-toastification"
import router from './routes'
import store from './store'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(ViewUI);

import './assets/css/style.css'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "vue-toastification/dist/index.css";
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import 'view-design/dist/styles/iview.css';

Vue.component('pagination', require('laravel-vue-pagination'));

Vue.use(Toast, {
  maxToasts: 2,
  transition: "Vue-Toastification__fade",
  closeButton: false,
  timeout: 3000,
  newestOnTop: true,
  position: "bottom-right",
  hideProgressBar: true,
  pauseOnHover: true
});

Vue.config.productionTip = false

new Vue({
  router, store,
  render: h => h(App)
}).$mount('#app')
