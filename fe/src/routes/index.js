import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/auth/Login.vue'
import Registration from '@/components/auth/Registration.vue'
import AdminDashboard from '@/components/admin/Dashboard.vue'
import AdminBooks from '@/components/admin/Books.vue'
import AdminBorrowedBooks from '@/components/admin/BorrowedBooks.vue'
import AdminRegistration from '@/components/admin/AdminRegistration.vue'
import AdminNewUsers from '@/components/admin/NewUsers.vue'
import Dashboard from '@/components/user/Dashboard.vue'
import Books from '@/components/user/Books.vue'
import BorrowedBooks from '@/components/user/BorrowedBooks.vue'
import Admin from '@/components/Admin.vue'
import User from '@/components/User.vue'
import NotFound from '@/components/404.vue'

Vue.use(VueRouter)

const routes = [
      {
        path: '/', 
        name: 'Login',
        component: Login,
        meta: { hasUser: true}
      },
      {
        path: '/register', 
        name: 'registration',
        component: Registration
      },
      {
        path: '/admin',
        name: 'AdminHome',
        component: Admin,
        meta: { isAdmin: true, requiresLogin: true },
        children: [
          {
          path:'dashboard',
          name: 'adminDashboard',
          components:{
            adminDashboard: AdminDashboard
            }
          },
          {
          path:'register',
          name: 'adminRegistration',
          components:{
            adminRegistration: AdminRegistration
            }
          },
          {
          path:'books',
          name: 'adminBooks',
          components:{
            adminBooks: AdminBooks
            }
          },
          {
          path:'borrowed-books',
          name: 'adminBorrowedBooks',
          components:{
            adminBorrowedBooks: AdminBorrowedBooks
            }
          },
          {
          path:'newusers',
          name: 'adminNewUsers',
          components:{
            adminNewUsers: AdminNewUsers
            }
          },
        ]
      },
      {
        path: '/user',
        name: 'UserHome',
        component: User,
        meta: { isUser: true, requiresLogin: true },
        children: [
          {
          path:'dashboard',
          name: 'dashboard',
          components:{
            dashboard: Dashboard
            },
          },
          {
          path:'books',
          name: 'books',
          components:{
            books: Books
            },
          },
          {
          path:'mybooks',
          name: 'borrowedbooks',
          components:{
            borrowedbooks: BorrowedBooks
            },
          },
        ]
      },
      {
        path: '*',
        component: NotFound
      }
    ]

    const router = new VueRouter({
      mode: 'history',
      base: process.env.BASE_URL,
      routes
    })

    router.beforeEach((to, from, next) => {
      if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem('auth')){
        next({name: 'Login'});
      }
      else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isAdmin')) {
          next({ name: "AdminHome" });
      } 
      else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isUser')) {
          next({ name: "UserHome" });
      }
      else if (to.matched.some((record) => record.meta.isAdmin) && localStorage.getItem('auth') && localStorage.getItem('isUser')) {
        next({ name: "UserHome" });
      } 
      else if (to.matched.some((record) => record.meta.isUser) && localStorage.getItem('auth') && localStorage.getItem('isAdmin')) {
          next({ name: "AdminHome" });
      } 
      else {
        next();
      }
    });
    
    export default router