import API from '../base'

export default {
  namespaced: true,
  state: {
    books: {},
    borrowedbooks: {},
    categories: {}
  },
  mutations: {
    SET_BOOKS(state, data) {
      state.books = data
    },
    SET_BORROWED_BOOKS(state, data) {
      state.borrowedbooks = data
    },
    SET_CATEGORIES(state, data) {
      state.categories = data
    },
    SET_UPDATE_BOOK(state, data){
      state.books = data
    }
  },
  actions: {
    async getBooks({commit}, page){
      const res = await API.get(`/admin/books?page=${page}`).then(res => {
        commit('SET_BOOKS', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getCategories({commit}, data){
      const res = await API.get('/admin/categories', data).then(res => {
        commit('SET_CATEGORIES', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getUserBooks({commit}, data){
      const res = await API.get('/user/books', data).then(res => {
        commit('SET_BOOKS', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async searchBook({commit}, {page, data}){
      const res = await API.post(`admin/search?page=${page}`, data).then(res => {
        commit('SET_BOOKS', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getUserBorrowedBooks({commit}, page){
      const res = await API.get(`/user/borrowedbooks?page=${page}`).then(res => {
        commit('SET_BORROWED_BOOKS', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getBorrowedBooks({commit}, page){
      const res = await API.get(`/admin/borrowedbooks?page=${page}`).then(res => {
        commit('SET_BORROWED_BOOKS', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async getUserCategories({commit}, data){
      const res = await API.get('/user/categories', data).then(res => {
        commit('SET_CATEGORIES', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async addBook({commit}, data){
      const res = await API.post('/admin/newbook', data).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })
  
      return res;
    },
    async borrowBook({commit}, data){
      const res = await API.post('/user/borrow', data).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })
  
      return res;
    },
    async updateBook({commit}, data){
          const res = await API.put(`/admin/books/${data.id}`, data).then(res => {
            commit('SET_UPDATE_BOOK', data)
            return res;
          }).catch(err => {
            return err.response;
          })

            return res;
        },
    async deleteBookRecord({commit}, id){
        const res = await API.delete(`/admin/books/destroy/${id}`).then(res => {
  
          return res;
        }).catch(err => {
         return err.response;
        })
  
        return res;
      },
    async returnBook({commit}, id){
        const res = await API.delete(`/user/return/${id}`).then(res => {
  
          return res;
        }).catch(err => {
         return err.response;
        })
  
        return res;
      },
  }
}