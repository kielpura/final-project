import API from '../base/'

export default {
  namespaced: true,
  state: {
    user: [],
    admin: [],
    useraccount: [],
    adminaccount: [],
    token: localStorage.getItem('auth') || ''
  },
  mutations: {
    PUSH_NEW_USER(state, data){
      state.user.push(data)
    },
    SET_ADMIN_ACC(state, data){
      state.adminaccount = data
      const bearer_token = localStorage.getItem('auth') || ''
      API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_USER_ACC(state, data){
      state.useraccount = data
      const bearer_token = localStorage.getItem('auth') || ''
      API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_USER(state, data) {
      state.user = data
    },
    SET_ADMIN(state, data) {
      state.admin = data
    },
    SET_AUTH_ADMIN_TOKEN(state, token) {
     localStorage.setItem('auth', token)
     localStorage.setItem('isAdmin', 'true')
     state.token = token

     const bearer_token = localStorage.getItem('auth') || ''
     API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_AUTH_USER_TOKEN(state, token) {
     localStorage.setItem('auth', token)
     localStorage.setItem('isUser', 'true')
     state.token = token

     const bearer_token = localStorage.getItem('auth') || ''
     API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    UNSET_ADMIN(state){
     localStorage.removeItem('auth');
     localStorage.removeItem('isAdmin');
     state.token = ''
     state.user = ''

     API.defaults.headers.common['Authorization'] = ''
   }, 
    UNSET_USER(state){
     localStorage.removeItem('auth');
     localStorage.removeItem('isUser');
     state.token = ''
     state.user = ''

     API.defaults.headers.common['Authorization'] = ''
   } 
  },
  actions: {
    async loginAdminAccount({commit}, payload){
      const res = await API.post('/auth/admin/login', payload).then(res => {
        commit('SET_ADMIN_ACC', res.data)
        commit('SET_AUTH_ADMIN_TOKEN', res.data.access_token)

        return res;
      }).catch(err => {
       return err
      })

      return res;
    },
    async logoutAdmin({commit}){
     const res = await API.post('auth/admin/logout?token=' + localStorage.getItem('auth')).then(response => {
       commit('UNSET_ADMIN')
       return response
     }).catch(error => {
       return error.response
     });

     return res;
   },
    async loginUserAccount({commit}, payload){
      const res = await API.post('/auth/user/login', payload).then(res => {
        commit('SET_USER_ACC', res.data)
        commit('SET_AUTH_USER_TOKEN', res.data.access_token)

        return res;
      }).catch(err => {
       return err
      })

      return res;
    },
    async logoutUser({commit}){
     const res = await API.post('auth/user/logout?token=' + localStorage.getItem('auth')).then(response => {
       commit('UNSET_USER')
       return response
     }).catch(error => {
       return error.response
     });

     return res;
   },
   async createAdminAccount({commit}, payload){
    const res = await API.post('/auth/admin/store', payload).then(res => {
      commit('PUSH_NEW_USER', payload)
      return res;
    }).catch(err => {
     return err.response;
    })

    return res;
  },
   async createUserAccount({commit}, payload){
    const res = await API.post('/auth/user/store', payload).then(res => {
      commit('PUSH_NEW_USER', payload)
      return res;
    }).catch(err => {
     return err.response;
    })

    return res;
  },
  async checkAuthUser({commit}) {
    const res = await API.post('auth/user/me?token=' + localStorage.getItem('auth')).then(res => {
      commit('SET_USER_ACC', res.data)

      return res;
    }).catch(error => {
      return error.response;
    })

    return res;
   },
  async checkAuthAdmin({commit}) {
    const res = await API.post('auth/admin/me?token=' + localStorage.getItem('auth')).then(res => {
      commit('SET_ADMIN_ACC', res.data)

      return res;
    }).catch(error => {
      return error.response;
    })

    return res;
   },
  },
}