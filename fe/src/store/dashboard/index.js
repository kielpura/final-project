import API from '../base'

export default {
  namespaced: true,
  state: {
    borrowedbooks: {},
  },
  mutations: {
    SET_MOST_BORROWED_BOOKS(state, data) {
      state.borrowedbooks = data
    }
  },
  actions: {
    async getMostBorrowedBooks({commit}, data){
      const res = await API.get('/admin/mostborrowed', data).then(res => {
        commit('SET_MOST_BORROWED_BOOKS', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
  }
}