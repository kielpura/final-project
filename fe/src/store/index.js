import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import book from './book'
import dashboard from './dashboard'
import newuser from './newuser'

Vue.use(Vuex);

export default new Vuex.Store({
 modules: {
  auth, book, newuser, dashboard
 }
})

