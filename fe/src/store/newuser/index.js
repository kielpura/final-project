import API from '../base'

export default {
  namespaced: true,
  state: {
    users: {}
  },
  mutations: {
    SET_NEW_USER(state, data) {
      state.users = data
    },
    SET_UPDATE_USER(state, data){
      state.users = data
    }
  },
  actions: {
    async getNewUsers({commit}, page){
      const res = await API.get(`/admin/newusers?page=${page}`).then(res => {
        commit('SET_NEW_USER', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async approveUser({commit}, data){
      const res = await API.put(`/admin/users/${data.id}`, data).then(res => {
        commit('SET_UPDATE_USER', data)
        return res;
      }).catch(err => {
        return err.response;
      })

        return res;
    },
    async deleteUser({commit}, id){
      const res = await API.delete(`/admin/users/destroy/${id}`).then(res => {

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
  }
}